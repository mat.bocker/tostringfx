/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tostringfx;

/**
 *
 * @author Mateus
 */
public class Magia {
    private String nome;
    private int consumo;
    
    
    public Magia(String nom,int consum)
    {
        this.nome=nom;
        this.consumo=consum;
    }

    public String getNome() {
        return nome;
    }

    public int getConsumo() {
        return consumo;
    }
    
    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setConsumo(int consumo) {
        this.consumo = consumo;
    }
    
    
}

