/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tostringfx;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

/**
 *
 * @author Mateus
 */
public class TelaStringController implements Initializable {
    @FXML
    private TextField nome1;
    @FXML
    private TextField nome2;
    @FXML
    private TextField vida1;
    @FXML
    private TextField vida2;
    @FXML
    private TextField mana1;
    @FXML
    private TextField mana2;
    @FXML
    private TextField magia11;
    @FXML
    private TextField magia12;
    @FXML
    private TextField magia21;
    @FXML
    private TextField magia22;
    @FXML
    private TextField magia31;
    @FXML
    private TextField magia32;
    @FXML
    private TextField magia41;
    @FXML
    private TextField magia42;
    @FXML
    private Button add1;
    @FXML
    private Button add2;
    @FXML
    private TextField consumo11;
    @FXML
    private TextField consumo12;
    @FXML
    private TextField consumo21;
    @FXML
    private TextField consumo22;
    @FXML
    private TextField consumo31;
    @FXML
    private TextField consumo32;
    @FXML
    private TextField consumo41;
    @FXML
    private TextField consumo42;
    @FXML
    private TextField nomejg;
    @FXML
    private TextField posmg;
    @FXML
    private Button usarmg;
    @FXML
    private TextField nomejgi;
    @FXML
    private TextField item;
    @FXML
    private Button additem;
    @FXML
    private Button removeitem;
    @FXML
    private TextField nomejgs;
    @FXML
    private Button inventario;
    @FXML
    private Button status;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }
    Jogador player1 = new Jogador();
    Jogador player2 = new Jogador();
    @FXML
    private void addjogador1()
    {
        player1.setNome(nome1.getText());
        player1.setVida(Integer.parseInt(vida1.getText()));
        player1.setMana(Integer.parseInt(mana1.getText()));
        player1.setMagia(magia11.getText(),Integer.parseInt(consumo11.getText()),0);
        player1.setMagia(magia21.getText(),Integer.parseInt(consumo21.getText()),1);
        player1.setMagia(magia31.getText(),Integer.parseInt(consumo31.getText()),2);
        player1.setMagia(magia41.getText(),Integer.parseInt(consumo41.getText()),3);
    }
    @FXML
    private void addjogador2()
    {
        player2.setNome(nome2.getText());
        player2.setVida(Integer.parseInt(vida2.getText()));
        player2.setMana(Integer.parseInt(mana2.getText()));
        player2.setMagia(magia12.getText(),Integer.parseInt(consumo12.getText()),0);
        player2.setMagia(magia22.getText(),Integer.parseInt(consumo22.getText()),1);
        player2.setMagia(magia32.getText(),Integer.parseInt(consumo32.getText()),2);
        player2.setMagia(magia42.getText(),Integer.parseInt(consumo42.getText()),3);
    }
    @FXML
    private void usarMagi()
    {
        int x,stat;
        String jg;
        jg=nomejg.getText();
        x=Integer.parseInt(posmg.getText());
        if(jg.equals(player1.getNome()))
        {
           player1.usarMagia(x);
        }
        else if(jg.equals(player2.getNome()))
        {
           player2.usarMagia(x);
        }
        else
        {
            System.out.println("Nenhum jogador encontrado!");
        }
    }
    @FXML
    private void adcitem()
    {
        String x,jg;
        jg=nomejgi.getText();
        x=item.getText();
        if(jg.equals(player1.getNome()))
        {
           player1.additem(x);
        }
        else if(jg.equals(player2.getNome()))
        {
           player2.additem(x);
        }
        else
        {
            System.out.println("Nenhum jogador encontrado!");
        }
        
    }
    @FXML
    private void removeitem()
    {
        String x,jg;
        jg=nomejgi.getText();
        x=item.getText();
        if(jg.equals(player1.getNome()))
        {
            player1.removeitem(x);
        }
        else if(jg.equals(player2.getNome()))
        {
            player2.removeitem(x);
        }
        else
        {
            System.out.println("Nenhum jogador encontrado!");
        }
       
    }
    @FXML
    private void mostrarInv()
    {
        String jg;
        jg=nomejgs.getText();
        if(jg.equals(player1.getNome()))
        {
           player1.monstrarInv();
        }
        else if(jg.equals(player2.getNome()))
        {
           player2.monstrarInv();
        }
        else
        {
            System.out.println("Nenhum jogador encontrado!");
        }
        
    }
    @FXML
    private void status()
    {
        String jg;
        jg=nomejgs.getText();
        if(jg.equals(player1.getNome()))
        {
           System.out.println(player1);
        }
        else if(jg.equals(player2.getNome()))
        {
           System.out.println(player2);
        }
        else
        {
            System.out.println("Nenhum jogador encontrado!");
        }
        
    }    
    
}
