/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tostringfx;

/**
 *
 * @author Mateus
 */
public class Inv {
    private int limite=10,i,x=0;
    private String[] itemEquipados = new String[5];
    private String[] items =  new String[limite];
    
    public Inv()
    {
        for(i=0;i<limite;i++)
        {
            items[i]="";
        }
        itemEquipados[0]="Capacete";
        itemEquipados[1]="Peitoral";
        itemEquipados[2]="Calça";
        itemEquipados[3]="Bota";
        itemEquipados[4]="Espada";
    }
    public void adicionarItem(String item)
    {
        for(i=0;i<limite;i++)
        {
            if("".equals(items[i]))
            {
                items[i]=item;
                i=limite;
                System.out.println("Item adicionado!");
            }
            else if(i==(limite-1)){
                System.out.println("Inventario cheio!");
            }
        }
    }

    public int getLimite() {
        return limite;
    }

    public String[] getItemEquipados() {
        return itemEquipados;
    }

    public String getItems(int i) {
        return items[i];
    }
    
    
    public void removerItem(String item)
    {
         for(i=0;i<limite;i++)
         {  
            if(items[i].equals(item))
            {
                items[i]="";
                i=limite;
                System.out.println("Item removido!");
            }
            else if(i==(limite-1)){
                System.out.println("Item não encontrado");
            }
        }
    }
    
}
