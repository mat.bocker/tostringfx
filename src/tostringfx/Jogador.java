/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tostringfx;

/**
 *
 * @author Mateus
 */
public class Jogador {
    private int vida;
    private int mana;
    private String nome;
    private Magia[] magias =  new Magia[4];
    private Inv inventario = new Inv();
     
    public int getVida() {
        return vida;
    }

    public void setVida(int vida) {
        this.vida = vida;
    }

    public int getMana() {
        return mana;
    }

    public void setMana(int mana) {
        this.mana = mana;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setMagia(String nome,int custo,int x) {
        magias[x] = new Magia(nome,custo);
    }

    public Inv getInventario() {
        return inventario;
    }

    public void setInventario(String item) {
        inventario.adicionarItem(item);
    }
    
    public void usarMagia(int i)
    {
       if(mana>=magias[i].getConsumo())
       {
          mana-=magias[i].getConsumo();
          System.out.println("Mana usada:"+magias[i].getConsumo()+"\nMana Atual:"+mana);
       }
       else
       {
          System.out.println("Mana insuficiente!");
       }
         
    }
    
    public void monstrarInv()
    {
        int o;
        for(o=0;o<inventario.getLimite();o++)
        {
            if(!("".equals(inventario.getItems(o))))
            {
                System.out.println(o+" = "+inventario.getItems(o));
            }
        }
    }
    public void additem(String item){
        inventario.adicionarItem(item);
    }
    
    public void removeitem(String item){
        inventario.removerItem(item);
    } 
    
    @Override
    public String toString(){
        return "nome: " + nome + "\nvida: " + vida + "\nmana: " + mana;
    }
}
